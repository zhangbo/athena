
/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../MuonSegmentOverlapRemovalTool.h"

using namespace Muon;

DECLARE_COMPONENT(MuonSegmentOverlapRemovalTool)
