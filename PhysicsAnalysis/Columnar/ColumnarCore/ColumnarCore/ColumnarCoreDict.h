/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/


#ifndef COLUMNAR_CORE_COLUMNAR_CORE_DICT_H
#define COLUMNAR_CORE_COLUMNAR_CORE_DICT_H

#include <ColumnarCore/ContainerId.h>

#include <ColumnarCore/ColumnarTool.h>

#endif
