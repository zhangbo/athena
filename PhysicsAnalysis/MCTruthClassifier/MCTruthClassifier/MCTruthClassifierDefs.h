/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDEFS_H
#define MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDEFS_H
/********************************************************************

NAME:     MCTruthClassifierDefs.h
PACKAGE:  atlasoff/PhysicsAnalysis/MCTruthClassifier

AUTHORS:  O. Fedin
CREATED:  Oct 2007

For more information, please see the twiki page:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCTruthClassifier
********************************************************************
Updated Feb 2024 by Andrii Verbytskyi <andrii.verbytskyi@mpp.mpg.de>
********************************************************************/
#include <vector>
#include <string>
namespace MCTruthPartClassifier {

#include "TruthUtils/TruthClasses.h"
  // Ensure that the enums are available from ROOT
  struct ROOT6_NamespaceAutoloadHook{};
}

#endif // MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDEFS_H
