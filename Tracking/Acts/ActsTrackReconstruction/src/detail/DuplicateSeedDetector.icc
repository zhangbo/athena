/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "src/detail/MeasurementIndex.h"

namespace ActsTrk::detail {

  inline void DuplicateSeedDetector::newTrajectory() {
    if (m_disabled || m_foundSeeds == 0 || m_nextSeed == m_nUsedMeasurements.size())
      return;

    auto beg = m_nUsedMeasurements.begin();
    if (m_nextSeed < m_nUsedMeasurements.size()) {
      std::advance(beg, m_nextSeed);
    }

    std::fill(beg, m_nUsedMeasurements.end(), 0ul);
  }

  inline void DuplicateSeedDetector::addMeasurement(const ActsTrk::ATLASUncalibSourceLink &sl, const MeasurementIndex& measurementIndex) {
    if (m_disabled || m_nextSeed == m_nUsedMeasurements.size())
      return;

    std::size_t hitIndex = measurementIndex.index(ActsTrk::getUncalibratedMeasurement(sl));
    if (!(hitIndex < m_seedIndex.size()))
      return;

    for (index_t iseed : m_seedIndex[hitIndex]) {
      assert(iseed < m_nUsedMeasurements.size());

      if (iseed < m_nextSeed || m_isDuplicateSeed[iseed])
        continue;

      if (++m_nUsedMeasurements[iseed] >= m_nSeedMeasurements[iseed]) {
        assert(m_nUsedMeasurements[iseed] == m_nSeedMeasurements[iseed]);  // shouldn't ever find more
        m_isDuplicateSeed[iseed] = true;
      }
      ++m_foundSeeds;
    }
  }

  // For complete removal of duplicate seeds, assumes isDuplicate(iseed) is called for monotonically increasing typeIndex,iseed.
  inline bool DuplicateSeedDetector::isDuplicate(std::size_t typeIndex, index_t iseed) {
    if (m_disabled)
      return false;

    if (typeIndex < m_seedOffset.size()) {
      iseed += m_seedOffset[typeIndex];
    }

    assert(iseed < m_isDuplicateSeed.size());
    // If iseed not increasing, we will miss some duplicate seeds, but won't exclude needed seeds.
    if (iseed >= m_nextSeed) {
      m_nextSeed = iseed + 1;
    }

    return m_isDuplicateSeed[iseed];
  }

}  // namespace ActsTrk::detail
